const express = require(`express`);
const router = express.Router();
const { Customer } = require(`../models/customers`);
const _ = require(`lodash`);
const Joi = require(`joi`);
const jwt = require(`jsonwebtoken`);
const config = require(`config`);

router.post(`/`, async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }
    let customer = await Customer.findOne({ emailAddress: req.body.emailAddress });

    if (!customer) {
        return res.status(400).send(`Invalid email or password.`)
    }

    if (req.body.passwordHash != customer.passwordHash) {
        return res.status(400).send(`Invalid email or password.`)
    }

    if (!customer.isVerified) {
        return res.status(400).send(`User is not verified!`)
    }

    const token = jwt.sign({ _id: customer._id, isAdmin: customer.isAdmin }, config.get(`jwtPrivateKey`));
    res.send(token)


});

function validate(req) {

    const schema = {

        emailAddress: Joi.string().required(),
        passwordHash: Joi.string().required()

    };
    return Joi.validate(req, schema);

}

module.exports = router;
