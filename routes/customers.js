
const auth = require(`../middleware/auth`);
const admin = require(`../middleware/admin`);
const Joi = require(`joi`);
const express = require(`express`);
const router = express.Router();
const Fawn = require(`fawn`);
const nodemailer = require(`nodemailer`);
const jwt = require(`jsonwebtoken`)
const config = require(`config`);
const _ = require(`lodash`);
const { Customer, validate } = require(`../models/customers`);

async function sendEmail(customerEmail, token, url) {

    nodemailer.createTestAccount((err, account) => {
        let transporter = nodemailer.createTransport({
            host: `smtp.gmail.com`,
            port: 587,
            secure: false,
            auth: {
                user: `prestatyn.tourism@gmail.com`,
                pass: config.get(`emailPassword`)
            }
        });

        let mailOptions = {
            from: '"Prestatyn App" <prestatyn.tourism@gmail.com>',
            to: customerEmail,
            subject: `Please confirm your account.`,
            html: `<h3>You recently signed up for an account on the Prestatyn App. Please <a href="https://${url}/api/confirmation/${token}">click here</a> to activate your account and enjoy the sunshine! ☀</h3><p>If you did not sign up for the Prestatyn App, please disregard this email.</p>`
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log(`Message sent: %s`, info.messageId);
        });
    });

};

router.get(`/`, [auth, admin], async (req, res) => {

    const customer = await Customer
        .find()
        .sort({ name: 1 })
        .select(`-passwordHash`);

    res.send(customer);

});

router.get(`/me`, auth, async (req, res) => {

    const customer = await Customer
        .findById(req.customer._id)
        .select(`-passwordHash -isAdmin -isVerified`);

    res.send(customer);


});

router.get(`/:id`, [auth, admin], async (req, res) => {

    const customer = await Customer
        .findById(req.params.id)
        .select(`-passwordHash`)

    if (customer == null) {

        return res.status(404).send(`Cannot find specified record!`);

    }

    res.send(customer);

});

router.post(`/`, async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }
    let customer = await Customer.findOne({ emailAddress: req.body.emailAddress });

    if (customer) {
        if (customer.isVerified == true) {
            return res.status(400).send(`User already registered.`)
        }
        else {
            await Customer.findOneAndRemove({ _id: customer.id }, { useFindAndModify: false })
        }
    }

    customer = new Customer(_.pick(req.body, [`firstname`, `surname`, `emailAddress`, `isVerified`, `passwordHash`, `isAdmin`]));

    new Fawn.Task()
        .save(`customers`, customer)
        .run();

    res.send(_.pick(customer, [`firstname`, `surname`, `emailAddress`, `isVerified`]));


    jwt.sign({ _id: customer.id }, config.get(`jwtEmailSecret`), { expiresIn: `1d` }, (err, token) => {
        sendEmail(customer.emailAddress, token, req.get('host'));
    });

});

router.put(`/:id`, [auth, admin], async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }
    const customer = await Customer.findOneAndUpdate({ _id: req.params.id }, {
        $set: _.pick(req.body, [`firstname`, `surname`, `emailAddress`, `isVerified`, `passwordHash`, `isAdmin`])
    }, { new: true, useFindAndModify: false });

    if (customer == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }
    res.send(_.pick(customer, [`firstname`, `surname`, `emailAddress`, `isVerified`, `isAdmin`]));

});


function validatePassword(password) {

    const schema = {

        passwordHash: Joi.string().required(),

    };
    return Joi.validate(password, schema);

}


router.patch(`/me`, auth, async (req, res) => {

    const { error } = validatePassword(req.body);

    if (error) {
        return res.status(400).send(error.message);
    };

    await Customer.findOneAndUpdate({ _id: req.customer._id }, {
        $set: {
            passwordHash: req.body.passwordHash
        }
    }, { new: true, useFindAndModify: false });

    res.send(`Password changed.`);


});

router.patch(`/:id`, [auth, admin], async (req, res) => {

    var doc = {};
    if (req.body.firstname) { doc.firstname = req.body.firstname };
    if (req.body.surname) { doc.surname = req.body.surname };
    if (req.body.emailAddress) { doc.emailAddress = req.body.emailAddress };
    if (req.body.isVerified) { doc.isVerified = req.body.isVerified };
    if (req.body.passwordHash) { doc.passwordHash = req.body.passwordHash };
    if (req.body.isAdmin) { doc.isAdmin = req.body.isAdmin };

    const customer = await Customer.findOneAndUpdate({ _id: req.params.id }, {
        $set: doc
    }, { new: true, useFindAndModify: false });

    if (customer == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }
    res.send(_.pick(customer, [`firstname`, `surname`, `emailAddress`, `isVerified`, `isAdmin`]));

});

router.delete(`/me`, auth, async (req, res) => {

    const customer = await Customer.findOneAndRemove({ _id: req.customer._id }, { useFindAndModify: false })
    res.send(_.pick(customer, [`firstname`, `surname`, `emailAddress`, `isVerified`, `isAdmin`]));

});

router.delete(`/:id`, [auth, admin], async (req, res) => {

    const customer = await Customer.findOneAndRemove({ _id: req.params.id }, { useFindAndModify: false })

    if (customer == null) {
        res.status(404).send(`Cannot find specified record!`)
        return;
    }

    res.send(_.pick(customer, [`firstname`, `surname`, `emailAddress`, `isVerified`, `isAdmin`]));

});

module.exports = router;