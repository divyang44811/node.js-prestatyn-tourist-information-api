
const auth = require(`../middleware/auth`);
const admin = require(`../middleware/admin`);
const express = require(`express`);
const router = express.Router();
const Fawn = require(`fawn`);
const _ = require(`lodash`);
const { Review, validate } = require(`../models/reviews`);
const { Customer } = require(`../models/customers`)
const { Activity } = require(`../models/activities`)
const { Hotel } = require(`../models/hotels`)
const { Restaurant } = require(`../models/restaurants`)

router.get(`/`, [auth, admin], async (req, res) => {

    const review = await Review
        .find()
        .sort({ name: 1 });

    res.send(review);

});

router.get(`/me`, auth, async (req, res) => {

    const review = await Review
        .find({ customer: req.customer._id });

    if (review == null) {
        res.status(404).send(`No reviews to show.`);
        return;
    }

    res.send(review);

});

router.get(`/:id`, async (req, res) => {

    const review = await Review
        .findById(req.params.id)
        .populate(`customer`, `-_id -passwordHash -isVerified -isAdmin -emailAddress`)

    if (review == null) {
        res.status(404).send(`Cannot find specified record.`);
        return;
    }

    res.send(review);

});

router.post(`/`, auth, async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }

    if (req.body.location.type == "activity") {
        var activity = await Activity.findById(req.body.location.id)

        if (!activity) {
            return res.status(404).send(`Cannot find specified activity.`)
        }
    }
    else if (req.body.location.type == "hotel") {
        var hotel = await Hotel.findById(req.body.location.id)

        if (!hotel) {
            return res.status(404).send(`Cannot find specified hotel.`)
        }
    }
    else if (req.body.location.type == "restaurant") {
        var restaurant = await Restaurant.findById(req.body.location.id)

        if (!restaurant) {
            return res.status(404).send(`Cannot find specified restaurant.`)
        }
    }

    const review = new Review(_.pick(req.body, [`score`, `location`, `title`, `detail`]));

    review.customer = req.customer._id;


    new Fawn.Task()
        .save(`reviews`, review)
        .run();

    if (hotel) {
        await Hotel.findOneAndUpdate({ _id: review.location.id }, {
            $push: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    else if (activity) {
        await Activity.findOneAndUpdate({ _id: review.location.id }, {
            $push: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    else if (restaurant) {
        await Restaurant.findOneAndUpdate({ _id: review.location.id }, {
            $push: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    res.send(review);

});

router.put(`/:id`, [auth, admin], async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }

    if (req.body.location.type == "activity") {
        var activity = await Activity.findById(req.body.location.id)

        if (!activity) {
            return res.status(404).send(`Cannot find specified activity.`)
        }
    }
    else if (req.body.location.type == "hotel") {
        var hotel = await Hotel.findById(req.body.location.id)

        if (!hotel) {
            return res.status(404).send(`Cannot find specified hotel.`)
        }
    }
    else if (req.body.location.type == "restaurant") {
        var restaurant = await Restaurant.findById(req.body.location.id)

        if (!restaurant) {
            return res.status(404).send(`Cannot find specified restaurant.`)
        }
    }

    const customer = await Customer.findById(req.body.customer);

    if (!customer) {
        res.status(404).send(`Cannot find specified customer.`);
        return;
    }

    const review = await Review.findOneAndUpdate({ _id: req.params.id }, {
        $set: _.pick(req.body, [`customer`, `location`, `score`, `title`, `detail`])
    }, { new: true, useFindAndModify: false });

    if (review == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }


    if (hotel) {
        await Hotel.findOneAndUpdate({ _id: review.location.id }, {
            $push: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    else if (activity) {
        await Activity.findOneAndUpdate({ _id: review.location.id }, {
            $push: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    else if (restaurant) {
        await Restaurant.findOneAndUpdate({ _id: review.location.id }, {
            $push: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }

    res.send(review);

});

router.patch(`/me/:id`, auth, async (req, res) => {

    var doc = {};
    if (req.body.score) { doc.score = req.body.score };
    if (req.body.title) { doc.title = req.body.title };
    if (req.body.detail) { doc.detail = req.body.detail };

    const review = await Review.findOneAndUpdate({ _id: req.params.id, customer: req.customer._id }, {
        $set: doc
    }, { new: true, useFindAndModify: false });

    if (review == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }
    res.send(review);

});


router.patch(`/:id`, [auth, admin], async (req, res) => {

    var doc = {};
    if (req.body.customer) { doc.customer = req.body.customer };
    if (req.body.score) { doc.score = req.body.score };
    if (req.body.title) { doc.title = req.body.title };
    if (req.body.detail) { doc.detail = req.body.detail };

    const review = await Review.findOneAndUpdate({ _id: req.params.id }, {
        $set: doc
    }, { new: true, useFindAndModify: false });

    if (review == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }
    res.send(review);

});

router.delete(`/me/:id`, auth, async (req, res) => {

    const review = await Review.findOneAndRemove({ _id: req.params.id, customer: req.customer._id }, { useFindAndModify: false })

    if (review == null) {
        res.status(404).send(`Cannot find specified record!`)
        return;
    }

    if (review.location.type == "hotel") {
        await Hotel.findOneAndUpdate({ _id: review.location.id }, {
            $pull: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    else if (review.location.type == "activity") {
        await Activity.findOneAndUpdate({ _id: review.location.id }, {
            $pull: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    else if (review.location.type == "restaurant") {
        await Restaurant.findOneAndUpdate({ _id: review.location.id }, {
            $pull: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }

    res.send(review);

});

router.delete(`/:id`, [auth, admin], async (req, res) => {

    const review = await Review.findOneAndRemove({ _id: req.params.id }, { useFindAndModify: false })

    if (review == null) {
        res.status(404).send(`Cannot find specified record!`)
        return;
    }

    if (review.location.type == "hotel") {
        await Hotel.findOneAndUpdate({ _id: review.location.id }, {
            $pull: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    else if (review.location.type == "activity") {
        await Activity.findOneAndUpdate({ _id: review.location.id }, {
            $pull: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }
    else if (review.location.type == "restaurant") {
        await Restaurant.findOneAndUpdate({ _id: review.location.id }, {
            $pull: { reviews: review._id }
        }, { new: true, useFindAndModify: false });
    }

    res.send(review);

});

module.exports = router;