
const auth = require(`../middleware/auth`);
const admin = require(`../middleware/admin`);
const express = require(`express`);
const router = express.Router();
const Fawn = require(`fawn`);
const _ = require(`lodash`);
const { Event, validate } = require(`../models/events`);

router.get(`/`, async (req, res) => {

    const event = await Event
        .find()
        .select(`_id name address category phoneNumber website position price thumbnailImage dateFrom dateTo`)
        .sort({ name: 1 });

    res.send(event);

});

router.get(`/:id`, async (req, res) => {

    const event = await Event
        .findById(req.params.id)

    if (event == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }

    res.send(event);

});

router.post(`/`, [auth, admin], async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }
    const event = new Event(_.pick(req.body, [`name`, `description`, `category`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`, `images`, `dateFrom`, `dateTo`]));


    new Fawn.Task()
        .save(`events`, event)
        .run();

    res.send(_.pick(location, [`_id`, `name`, `description`, `category`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`, `dateFrom`, `dateTo`]));

});

router.put(`/:id`, [auth, admin], async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }

    const event = await Event.findOneAndUpdate({ _id: req.params.id }, {
        $set: _.pick(req.body, [`name`, `description`, `address`, `category`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`, `images`, `dateFrom`, `dateTo`])
    }, { new: true, useFindAndModify: false });

    if (event == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }
    res.send(_.pick(req.body, [`name`, `description`, `address`, `category`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`, `dateFrom`, `dateTo`]));

});

router.patch(`/:id`, [auth, admin], async (req, res) => {

    var doc = {};
    if (req.body.name) { doc.name = req.body.name };
    if (req.body.description) { doc.description = req.body.description };
    if (req.body.address) { doc.address = req.body.address };
    if (req.body.phoneNumber) { doc.phoneNumber = req.body.phoneNumber };
    if (req.body.website) { doc.website = req.body.website };
    if (req.body.dateFrom) { doc.dateFrom = req.body.dateFrom };
    if (req.body.dateTo) { doc.dateTo = req.body.dateTo };
    if (req.body.position) { doc.position = req.body.position };
    if (req.body.price) { doc.price = req.body.price };
    if (req.body.category) { doc.category = req.body.category };

    var img = {}

    if (req.body.images) { img.images = req.body.images };

    var event = await Event.findOneAndUpdate({ _id: req.params.id }, {
        $set: doc
    }, { new: true, useFindAndModify: false });

    if (event == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    };

    if (img) {
        event = await Event.findOneAndUpdate({ _id: req.params.id }, {
            $addToSet: img
        }, { new: true, useFindAndModify: false });
    }

    res.send(_.pick(req.body, [`name`, `description`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`, `category`, `dateFrom`, `dateTo`]));

});

router.delete(`/:id`, [auth, admin], async (req, res) => {

    const event = await Event.findOneAndRemove({ _id: req.params.id }, { useFindAndModify: false })

    if (event == null) {
        res.status(404).send(`Cannot find specified record!`)
        return;
    }

    res.send(_.pick(req.body, [`name`, `description`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`, `category`, `dateFrom`, `dateTo`]));

});

module.exports = router;