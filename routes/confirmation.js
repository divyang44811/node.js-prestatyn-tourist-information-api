const express = require(`express`);
const router = express.Router();
const config = require(`config`);
const jwt = require(`jsonwebtoken`);
const { Customer } = require(`../models/customers`);

router.get(`/:token`, async (req, res) => {



    try {
        const { _id } = jwt.verify(req.params.token, config.get(`jwtEmailSecret`));

        console.log(_id)

        await Customer.findOneAndUpdate({ _id: _id }, {
            $set: {
                isVerified: true
            }
        }, { new: true, useFindAndModify: false });

        res.send(`<h1>Your account is now verified!</h1>`)
    }
    catch (err) {
        res.send(`Error when verifying customer.`, err)
    }


});


module.exports = router;