
const auth = require(`../middleware/auth`);
const admin = require(`../middleware/admin`);
const express = require(`express`);
const router = express.Router();
const Fawn = require(`fawn`);
const _ = require(`lodash`);
const { Activity, validate } = require(`../models/activities`);

router.get(`/`, async (req, res) => {

    let activity = await Activity
        .find()
        .select(`_id name address phoneNumber category website position category price thumbnailImage`)
        .sort({ name: 1 })
        .populate(`reviews`, `-_id score`);

    res.send(activity)

});

router.get(`/:id`, async (req, res) => {

    const activity = await Activity
        .findById(req.params.id)
        .populate({ path: `reviews`, select: `-_id -location`, populate: { path: `customer`, select: `-_id -emailAddress -passwordHash -isVerified -isAdmin` } })

    if (activity == null) {
        return res.status(404).send(`Cannot find specified record!`);
    }

    res.send(activity);

});

router.post(`/`, [auth, admin], async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }
    const activity = new Activity(_.pick(req.body, [`name`, `description`, `category`, `address`, `phoneNumber`, `website`, `openingTimes`, `thumbnailImage`, `position`, `price`, `images`]));


    new Fawn.Task()
        .save(`activities`, activity)
        .run();

    res.send(_.pick(activity, [`_id`, `name`, `description`, `category`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`]));

});

router.put(`/:id`, [auth, admin], async (req, res) => {

    const { error } = validate(req.body);

    if (error) {
        res.status(400).send(error.message);
        return;
    }

    const activity = await Activity.findOneAndUpdate({ _id: req.params.id }, {
        $set: _.pick(req.body, [`name`, `description`, `category`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `thumbnailImage`, `price`, `images`])
    }, { new: true, useFindAndModify: false });

    if (activity == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }
    res.send(_.pick(activity, [`_id`, `name`, `description`, `category`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`]));

});

router.patch(`/:id`, [auth, admin], async (req, res) => {

    var doc = {};
    if (req.body.name) { doc.name = req.body.name };
    if (req.body.description) { doc.description = req.body.description };
    if (req.body.address) { doc.address = req.body.address };
    if (req.body.phoneNumber) { doc.phoneNumber = req.body.phoneNumber };
    if (req.body.website) { doc.website = req.body.website };
    if (req.body.openingTimes) { doc.openingTimes = req.body.openingTimes };
    if (req.body.position) { doc.position = req.body.position };
    if (req.body.price) { doc.price = req.body.price };
    if (req.body.category) { doc.category = req.body.category };
    if (req.body.thumbnailImage) { doc.thumbnailImage = req.body.thumbnailImage };

    var img = {}

    if (req.body.images) { img.images = req.body.images };

    var activity = await Activity.findOneAndUpdate({ _id: req.params.id }, {
        $set: doc
    }, { new: true, useFindAndModify: false });

    if (activity == null) {
        res.status(404).send(`Cannot find specified record!`);
        return;
    }

    if (img) {
        activity = await Activity.findOneAndUpdate({ _id: req.params.id }, {
            $addToSet: img
        }, { new: true, useFindAndModify: false });
    }

    res.send(_.pick(activity, [`_id`, `name`, `description`, `category`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`]));

});

router.delete(`/:id`, [auth, admin], async (req, res) => {

    const activity = await Activity.findOneAndRemove({ _id: req.params.id }, { useFindAndModify: false })

    if (activity == null) {
        res.status(404).send(`Cannot find specified record!`)
        return;
    }

    res.send(_.pick(activity, [`_id`, `name`, `description`, `category`, `address`, `phoneNumber`, `website`, `openingTimes`, `position`, `price`]));

});

module.exports = router;