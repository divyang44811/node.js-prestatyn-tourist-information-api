
const mongoose = require(`mongoose`);
const Joi = require(`joi`);

const Activity = mongoose.model(`Activity`, new mongoose.Schema({

    name: {
        type: String,
        required: true,
        maxlength: 255,
    },
    description: {
        type: String,
        maxlength: 255,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    position: {
        lat: {
            type: Number,
            required: true
        },
        lon: {
            type: Number,
            required: true
        }
    },
    phoneNumber: {
        type: Number,
        minlength: 9,
        maxlength: 15
    },
    website: {
        type: String,
        match: [/@^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$@i/, `Please enter a valid website address.`]
    },
    openingTimes: {
        mon: {
            type: String
        },
        tue: {
            type: String
        },
        wed: {
            type: String
        },
        thu: {
            type: String
        },
        fri: {
            type: String
        },
        sat: {
            type: String
        },
        sun: {
            type: String
        }
    },
    reviews: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: `Review`
    }],
    price: {
        type: String,
        required: true,
        enum: [`FREE`, `£`, `££`, `£££`]
    },
    category: {
        type: String,
        required: true,
        enum: [`Beach`, `Shopping`, `Scenic Area`, `Hiking trail`, `Cinema`, `Museum`, `Castle`, `Church`, `Amusements`, `Play area`, `Swimming pool`, `Points of interest`, `Visitor Centre`, `Aquarium`, `Nature and wildlife`, `Sports and outdoors`]
    },
    thumbnailImage: {
        contentType: {
            type: String,
            enum: [`thumbnail`, `png`],
            required: true
        },
        imageData: {
            type: String,
            required: true
        }
    },
    images: [{
        contentType: {
            type: String,
            enum: [`thumbnail`, `png`]
        },
        imageData: {
            type: String
        }
    }]

}));

function validateActivity(activity) {

    const image = Joi.object().keys({
        contentType: Joi.string().required(),
        isThumbnail: Joi.boolean().required()
    })

    const schema = {
        name: Joi.string().max(255).required(),
        description: Joi.string().max(255).required(),
        address: Joi.string().required(),
        position: Joi.object().required().keys({
            lat: Joi.string().required(),
            lon: Joi.string().required()
        }),
        phoneNumber: Joi.string().min(9).max(15),
        website: Joi.string(),
        openingTimes: Joi.object().keys({
            mon: Joi.string(),
            tue: Joi.string(),
            wed: Joi.string(),
            thu: Joi.string(),
            fri: Joi.string(),
            sat: Joi.string(),
            sun: Joi.string()
        }),
        price: Joi.string().required(),
        type: Joi.string().required(),
        reviews: Joi.objectId(),
        thumbnailImage: Joi.object().required().keys({
            contentType: Joi.string().required(),
            imageData: Joi.string().required()
        }),
        images: Joi.array().items(image)
    };
    return Joi.validate(activity, schema);

}

exports.Activity = Activity;
exports.validate = validateActivity;