const mongoose = require(`mongoose`);
const Joi = require(`joi`);

const Review = mongoose.model(`Review`, new mongoose.Schema({

    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: `Customer`,
        required: true
    },
    location: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: `Location`,
            required: true
        },
        type: {
            type: String,
            enum: [`hotel`, `activity`, `restaurant`],
            required: true
        }
    },
    score: {
        type: Number,
        min: 0,
        max: 5,
        required: true
    },
    title: {
        type: String,
        maxlength: 50,
        required: false
    },
    detail: {
        type: String,
        minlength: 5,
        maxlenghth: 255
    }

}));

function validateReview(review) {
    const schema = {

        location: Joi.object().required().keys({
            id: Joi.objectId().required(),
            type: Joi.string().required()
        }),
        score: Joi.number().required().min(0).max(5),
        title: Joi.string().max(50),
        detail: Joi.string().min(5).max(255)

    };

    return Joi.validate(review, schema);
}

exports.Review = Review;
exports.validate = validateReview;