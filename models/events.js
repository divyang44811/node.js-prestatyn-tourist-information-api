
const mongoose = require(`mongoose`);
const Joi = require(`joi`);

const Event = mongoose.model(`Event`, new mongoose.Schema({

    name: {
        type: String,
        required: true,
        maxlength: 255,
    },
    address: {
        type: String,
        required: true
    },
    description: {
        type: String,
        maxlength: 255,
        required: true
    },
    position: {
        lat: {
            type: Number,
            required: true
        },
        lon: {
            type: Number,
            required: true
        }
    },
    phoneNumber: {
        type: Number,
        minlength: 9,
        maxlength: 15
    },
    website: {
        type: String,
        match: [/@^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$@i/, `Please enter a valid website address.`],
        minlength: 7
    },
    reviews: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: `Reviews`
    }],
    dateFrom: {
        type: Date,
    },
    dateTo: {
        type: Date
    },
    price: {
        type: String,
        required: true,
        enum: [`FREE`, `£`, `££`, `£££`]
    },
    category: {
        type: String,
        required: true,
        enum: [`Live music`, `Antiques`,`Car boot`,`Market`, `Theatre`,`Canival`,`Crafts`,`Sports/ outdoors` ]
    },
    thumbnailImage: {
        contentType: {
            type: String,
            enum: [`thumbnail`, `png`],
            required: true
        },
        imageData: {
            type: String,
            required: true
        }
    },
    images: [{
        contentType: {
            type: String,
            enum: [`thumbnail`, `png`]
        },
        imageData: {
            type: String
        }
    }]


}));

function validateEvent(event) {

    const image = Joi.object().keys({
        contentType: Joi.string(),
        imageData: Joi.string()
    })

    const schema = {
        name: Joi.string().max(255).required(),
        description: Joi.string().max(255),
        address: Joi.string().required(),
        position: Joi.object().required().keys({
            lat: Joi.string().required(),
            lon: Joi.string().required()
        }),
        phoneNumber: Joi.string().min(9).max(15).required(),
        price: Joi.number().required(),
        website: Joi.string().min(7),
        dateFrom: Joi.date(),
        dateTo: Joi.date(),
        reviews: Joi.objectId(),
        thumbnailImage: Joi.object().required().keys({
            contentType: Joi.string().required(),
            imageData: Joi.string().required()
        }),
        type: Joi.string().required(),
        images: Joi.array().items(image)
    };

    return Joi.validate(event, schema);

}

exports.Event = Event;
exports.validate = validateEvent;