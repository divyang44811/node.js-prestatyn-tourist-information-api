const mongoose = require(`mongoose`);
const Joi = require(`joi`);

const Customer = mongoose.model(`Customer`, new mongoose.Schema({

    firstname: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 255
    },
    surname: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 255
    },
    emailAddress: {
        type: String,
        required: true,
        unique: true,
        match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, `Please enter a valid email address.`]
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    passwordHash: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    }

}));

function validateCustomer(customer) {

    const schema = {

        firstname: Joi.string().required().min(2).max(255),
        surname: Joi.string().required().min(2).max(255),
        emailAddress: Joi.string().required(),
        passwordHash: Joi.string().required(),

    };
    return Joi.validate(customer, schema);

}

exports.Customer = Customer;
exports.validate = validateCustomer;