# Node.JS Prestatyn Tourist Information API

A RESTful API built in Node.JS and express. Designed for use with the ![Prestayn Tourist Information app built in C# Xamarin Forms](https://gitlab.com/Watermelonn/csharp-xamarin-prestatyn-tourism-app-ios-android).

## Dependencies

- npm
- ![mongoose](https://www.npmjs.com/package/mongoose)
- ![express](https://www.npmjs.com/package/express)
- ![express-async-errors](https://www.npmjs.com/package/express-async-errors)
- ![body-parser](https://www.npmjs.com/package/body-parser)
- ![joi](https://www.npmjs.com/package/joi)
- ![joi-objectid](https://www.npmjs.com/package/joi-objectid)
- ![fawn](https://www.npmjs.com/package/fawn)
- ![lodash](https://www.npmjs.com/package/lodash)
- ![jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
- ![nodemailer](https://nodemailer.com/about/)
- ![winston](https://www.npmjs.com/package/winston)
- ![helmet](https://www.npmjs.com/package/helmet)
- ![compression](https://www.npmjs.com/package/compression)