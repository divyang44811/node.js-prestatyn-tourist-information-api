const jwt = require(`jsonwebtoken`);
const config = require(`config`);

module.exports = function (req, res, next) {

    const token = req.header(`x-auth-token`);
    if (!token) {
        return res.status(401).send(`Insufficient privileges. No token provided`);
    }
    try {
        const decodedToken = jwt.verify(token, config.get(`jwtPrivateKey`));
        req.customer = decodedToken;
        next();
    }
    catch (err) {
        res.status(400).send(`Invalid token.`);
    }

}