const mongoose = require(`mongoose`);
const Fawn = require(`fawn`);
const config = require(`config`);

Fawn.init(mongoose);

module.exports = function () {

    mongoose.connect(config.get(`db`),  { useNewUrlParser: true })
        .then(() => console.log(`Connected to MongoDB...`))

};