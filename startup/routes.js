
const express = require(`express`);
const bodyParser = require(`body-parser`);
const activities = require(`../routes/activities`);
const hotels = require(`../routes/hotels`);
const restaurants = require(`../routes/restaurants`);
const events = require(`../routes/events`);
const customers = require(`../routes/customers`);
const reviews = require(`../routes/reviews`);
const confirmation = require(`../routes/confirmation`);
const auth = require(`../routes/auth`);
const error = require(`../middleware/error`);

module.exports = function (app) {

    app.use(bodyParser.json({ limit: '10mb' }));
    app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
    app.use(express.json());
    app.use(`/api/hotels`, hotels);
    app.use(`/api/restaurants`, restaurants);
    app.use(`/api/activities`, activities);
    app.use(`/api/events`, events);
    app.use(`/api/customers`, customers);
    app.use(`/api/reviews`, reviews);
    app.use(`/api/confirmation`, confirmation);
    app.use(`/api/auth`, auth);
    app.use(error);

};