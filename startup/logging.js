const winston = require(`winston`);
const config = require(`config`)
require(`winston-mongodb`)
require(`express-async-errors`);

module.exports = function () {

    winston.add(winston.transports.File, {filename:`./error.log`})
    winston.add(winston.transports.MongoDB,{
        db: config.get(`db`),
        level: `error`
    })

    process.on(`uncaughtException`, (err) => {
        winston.error(err);
        process.exit(1);
    });


    process.on(`unhandledRejection`, (err) => {
        winston.error(err);
        process.exit(1);
    });

};