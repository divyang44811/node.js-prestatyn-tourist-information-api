const config = require(`config`);

module.exports = function () {

    if (!config.get(`jwtPrivateKey`)) {

        throw new Error(`FATAL ERROR: jwtPrivateKey is not defined!`);

    };
    if (!config.get(`jwtEmailSecret`)) {

        throw new Error(`FATAL ERROR: jwtEmailSecret is not defined!`);
        
    };
    if (!config.get(`emailPassword`)) {

        throw new Error(`FATAL ERROR: emailPassword is not defined!`);

    };

};